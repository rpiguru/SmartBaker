# Smart Baker

## Components

- NodeMCU
 
    https://www.amazon.com/NodeMCU-ESP8266-ESP-12E-Development-Board/dp/B0741WPPDY/ref=sr_1_3?s=electronics&ie=UTF8&qid=1517534541&sr=1-3&keywords=nodemcu
    
    https://www.ebay.com/itm/Wireless-module-NodeMcu-Lua-WIFI-development-board-based-ESP8266-CP2102/152761175208?epid=15008830605&hash=item2391468ca8:g:F98AAOSwHUhaF7D4
    
- Quad Alphanumeric Display

    http://adafruit.com/product/1191

- MPU6050 
    
    https://www.amazon.com/MPU-6050-MPU6050-Accelerometer-Gyroscope-Converter/dp/B008BOPN40/ref=sr_1_2?ie=UTF8&qid=1517534747&sr=8-2&keywords=mpu6050

    https://www.ebay.com/itm/MPU-6050-6DOF-3-Axis-Gyroscope-Accelerometer-Module-for-Arduino-DIY/201415045005?hash=item2ee545af8d:g:OSwAAOSwDwtUm4WP

- K Type Thermocouple
    
    https://www.amazon.com/Adafruit-Thermocouple-Amplifier-MAX31855-breakout/dp/B00SK8NDAI/ref=sr_1_1?ie=UTF8&qid=1517534806&sr=8-1&keywords=k+type+thermocouple+max31855
    
    https://www.ebay.com/itm/Competitive-New-MAX31855-Module-K-Type-Thermocouple-Sensor-for-Arduino/192077472612?hash=item2cb8b5a364Ⓜmf9_zPwe8nigAPgBu5QFqWA

- MLX90614: 
    
    https://www.amazon.com/LM-YN-Thermometer-Non-contact-thermometer/dp/B01LYA41ST/ref=sr_1_2?ie=UTF8&qid=1517534959&sr=8-2&keywords=MLX90614
    
    https://www.ebay.com/itm/MLX90614ESF-Infrared-Thermometer-IR-Breakout-Board-Temperature-for-Arduino/232232932451?epid=1754172609&hash=item3612295463:g:yJgAAOSwA3dYmoVJ

- 3 Push Buttons
    
    https://www.amazon.com/Cylewet-6%C3%976%C3%979mm-Tactile-Arduino-CYT1052/dp/B06VY1WJ8Z/ref=sr_1_1?s=electronics&ie=UTF8&qid=1517534989&sr=1-1&keywords=push+button
    
- Buzzer
    
    https://www.amazon.com/Gikfun-Active-Magnetic-Continous-Arduino/dp/B01FVZQ6F6/ref=sr_1_2?s=electronics&ie=UTF8&qid=1517535033&sr=1-2&keywords=buzzer+3v+3
    
    https://robotdyn.com/buzzer-module.html

- 115 VAC 11 Amps Relay
    
    https://www.amazon.com/dp/B072TWZ28F/ref=sr_1_1_twi_col_4?s=electronics&ie=UTF8&qid=1517535242&sr=1-1&keywords=110vac+relay+module
    
    https://robotdyn.com/relay-module-1-relay-5v-30a.html

- MCP23008 I2C 8 Input/Output Port Expander

    https://www.adafruit.com/product/593

## Connection Guide
   
   
![Schematic View](schematic/smart_baker_schem.jpg)

![Breadboard View](schematic/smart_baker_bb.jpg)

| **MCP23008** | **NodeMCU**  |
|  :-------:   |   :------:   |
|     3.3V     |     3v3      |
|     GND      |     GND      |
|     SDA      |     D2       |
|     SCL      |     D1       |
|              |              |
| **MLX90614** |              |
|     3.3V     |     3v3      |
|     GND      |     GND      |
|     SDA      |     D2       |
|     SCL      |     D1       |
| **MPU6050**  |              |
|     3.3V     |     3v3      |
|     GND      |     GND      |
|     SDA      |     D2       |
|     SCL      |     D1       |
|   **LCD**    |              |
|     3.3V     |     3v3      |
|     GND      |     GND      |
|     SDA      |     D2       |
|     SCL      |     D1       |
| **Thermocouple** |          |
|     3.3V    |      3v3      |
|     GND     |      GND      |
|     CLK     |      D5       |
|     CS      |      D6       |
|     DO      |      D8       |
|  **Relay**  | **MCP23008**  |
|     3.3V    |      3v3      |
|     GND     |      GND      |
|   Signal    |      GP0      |

- Buttons

    * **Mode:** D7
    * **Up:** D4
    * **Down:** D3
    * **Buzzer:** D0
