/*
    Testing code of 7 segment LED
*/
#include <Arduino.h>
#include "MPU6050.h"

scaleddata mpu6050_values;

void setup() {
    Wire.begin();
    Serial.begin(9600);
 
    mpu6050Begin(MPU_addr);
    setMPU6050scales(MPU_addr, 0b00000000, 0b00010000);
}
 
void loop() {
    mpu6050_values = mpu6050Read(MPU_addr, false);
    Serial.print(" GyX = "); Serial.print(mpu6050_values.GyX);
    Serial.print(" °/s| GyY = "); Serial.print(mpu6050_values.GyY);
    Serial.print(" °/s| GyZ = "); Serial.print(mpu6050_values.GyZ);
    Serial.print(" °/s| Tmp = "); Serial.print(mpu6050_values.Tmp);
    Serial.print(" °C| AcX = "); Serial.print(mpu6050_values.AcX);
    Serial.print(" g| AcY = "); Serial.print(mpu6050_values.AcY);
    Serial.print(" g| AcZ = "); Serial.print(mpu6050_values.AcZ);Serial.println(" g");
    delay(1000);            // Wait 1 second and scan again
}
