
// ============================= Do Not Change These Values ==========================================================

#ifndef _BV
  #define _BV(bit) (1 << (bit))
#endif

// Pin Configuration
#define BUZZER_PIN              16
#define BTN_MODE_PIN            13
#define BTN_UP_PIN              2
#define BTN_DOWN_PIN            0

// MCP23008 Configuration
#define RELAY_CHANNEL           0                   // Channel number of the relay 

// EEPROM Configuration
#define EEPROM_TIME_ADDRESS     0
#define EEPROM_TEMP_ADDRESS     2

// Mode Definition
#define MODE_NORMAL             0
#define MODE_TIME               1
#define MOTE_AFTER_TIME         2
#define MODE_TEMP               3

#define MICRODELAY              100                 // Read the status of MODE button every 100ms

// State Definition
#define STATE_READY             0                   // Standby
#define STATE_HEATING           1                   // Heating State
#define STATE_REACHED           2                   // The temperature reaches to the threshold
#define STATE_OPENED            3                   // The One Half is opened.
#define STATE_FLIPPED           4                   // Baker flipped the half
#define STATE_COUNT_DOWN        5                   // Count down after closing & flipping the half
#define STATE_DONE              6                   // Finished.
