#include <Arduino.h>
#include <EEPROM.h>
#include "MPU6050.h"
#include <Adafruit_MLX90614.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"
#include "Adafruit_MAX31855.h"
#include "Adafruit_MCP23008.h"
#include "constant.h"

// ============================= Configure Parameters Here ============================================================
#define MODE_BUTTON_DELAY           2                   // User has to press MODE button for 2 seconds to switch mode.
#define BUZZER_FREQUENCY            400
#define BUZZER_FREQUENCY_ERROR      600
#define DEFAULT_COOK_TIME           150                 // Default cook time (2:30)
#define DEFAULT_THRESHOLD           390                 // Default threshold temperature(°F)
#define TEMP_CHECK_INTERVAL         30                  // Check temperature every 30 sec

// #define TEMP_THRESHOLD_HALF         212                 // Temperature threshold to detect if the half is opened/closed(°F)
#define TEMP_THRESHOLD_HALF         100                  // Temperature threshold to detect if the half is opened/closed(°F)

bool DEBUG = false;
// ====================================================================================================================


scaleddata mpu6050_values;
bool is_half_flipped = false;

Adafruit_MLX90614 mlx = Adafruit_MLX90614();
double mlx_temp_obj;
bool is_half_opened = false;

Adafruit_AlphaNum4 alpha4 = Adafruit_AlphaNum4();
bool is_lcd_shown = false;

Adafruit_MAX31855 thermocouple(14, 12, 15);
double thermocouple_temp = 0;

Adafruit_MCP23008 mcp23008;

// Internal Variables
int mode = MODE_NORMAL;
int state = STATE_READY;

int cook_time = 0;
int threshold = 0;

unsigned long last_mode_button_time = 0;
unsigned long count_down_start_time = 0;
unsigned long remaining_time = 0;
int cnt = 0;
unsigned long cnt_thermocouple = 0;
bool last_relay_state = false;
double last_thermocouple_temp = 10000;      // Give very large value for the initial logic.

unsigned long last_buzzer_time = 0;
int cnt_buzzer = 0;


unsigned long last_btn_mode_pressed_time = 0;
void on_btn_mode(){
    if (!digitalRead(BTN_MODE_PIN)) {
        if (millis() - last_btn_mode_pressed_time > 200){
            last_btn_mode_pressed_time = millis();
            last_mode_button_time = millis();
            Serial.println("MODE button is pressed");
        }
    }
    else {
        // Clear the MODE button timer.
        last_mode_button_time = 0;
    }
}

void save_threshold() {
    EEPROM.write(EEPROM_TEMP_ADDRESS, threshold / 256);
    EEPROM.write(EEPROM_TEMP_ADDRESS + 1, threshold % 256);
    EEPROM.commit();
}

void save_cook_time() {
    EEPROM.write(EEPROM_TIME_ADDRESS, cook_time / 256);
    EEPROM.write(EEPROM_TIME_ADDRESS + 1, cook_time % 256);
    EEPROM.commit();
}

unsigned long last_btn_up_time = 0;
void on_btn_up(){
    if  (millis() - last_btn_up_time > 200){
         last_btn_up_time = millis();
         if (mode == MODE_TEMP){
            if (threshold < 9999){          // Upper limit is 9999°F
                threshold++;
                save_threshold();
                Serial.print("Threshold: "); Serial.println(threshold);
            }
        }
        else if (mode == MODE_TIME){
            if (cook_time < 60 * 60){       // Upper limit is an hour
                cook_time++;
                save_cook_time();
                Serial.print("Cook Time: "); Serial.println(cook_time);
            }
        }
    }
}

unsigned long last_btn_down_time = 0;
void on_btn_down(){
    if  (millis() - last_btn_down_time > 200){
         last_btn_down_time = millis();
         if (mode == MODE_TEMP){
             if (threshold > 0){
                threshold--;
                save_threshold();
                Serial.print("Threshold: "); Serial.println(threshold);
            }
        }
        else if (mode == MODE_TIME){
            if (cook_time > 0){
                cook_time--;
                save_cook_time();
                Serial.print("Cook Time: "); Serial.println(cook_time);
            }
        }
    }
}

// void ICACHE_RAM_ATTR onTimerISR(){
//     if (last_mode_button_time > 0){
//         Serial.print(".");
//         if (millis() - last_mode_button_time > MODE_BUTTON_DELAY * 1000){
//             last_mode_button_time = 0;
//             // Switch the current mode.  NORMAL => TIME => AFTER_TIME => TEMP => NORMAL
//             if (mode == MODE_TEMP) mode = MODE_NORMAL;
//             else mode++;
//             Serial.print("Switched the current mode: "); Serial.println(mode);
//         }
//     }
// }

void displayString(String str) {
  for (int i=0; i < 4; i++) {
    alpha4.writeDigitAscii(i, str[i]);
  }
  alpha4.writeDisplay();
}


// void attachTimer(){
//     // HW Timer 0 is used by WiFi Functions, and only Timer1 can be used.
//     timer1_disable();
//     timer1_attachInterrupt(onTimerISR);
//     timer1_isr_init();
//     timer1_enable(TIM_DIV16, TIM_EDGE, TIM_LOOP);     // 5MHz (5 ticks/us - 1677721.4 us max)
//     timer1_write(clockCyclesPerMicrosecond() * MICRODELAY * 1111);
// }

void setup() {

    EEPROM.begin(512);

    Wire.begin();
    Serial.begin(9600);
 
    mpu6050Begin(MPU_addr);
    setMPU6050scales(MPU_addr, 0b00000000, 0b00010000);

    mlx.begin();

    mcp23008.begin();
    mcp23008.pinMode(RELAY_CHANNEL, OUTPUT);

    alpha4.begin(0x70);

    // attachTimer();
    attachInterrupt(BTN_MODE_PIN, on_btn_mode, CHANGE);
    attachInterrupt(BTN_UP_PIN, on_btn_up, FALLING);
    attachInterrupt(BTN_DOWN_PIN, on_btn_down, FALLING);

    threshold = EEPROM.read(EEPROM_TEMP_ADDRESS) * 256 + EEPROM.read(EEPROM_TEMP_ADDRESS + 1);
    cook_time = EEPROM.read(EEPROM_TIME_ADDRESS) * 256 + EEPROM.read(EEPROM_TIME_ADDRESS + 1);
    if (threshold == 0xFFFF){
        threshold = DEFAULT_THRESHOLD;
        save_threshold();
    }
    if (cook_time == 0xFFFF){
        cook_time = DEFAULT_COOK_TIME;
        save_cook_time();
    }

    ESP.wdtDisable();
    ESP.wdtEnable(WDTO_8S);
}
 
void loop() {
    if (last_mode_button_time > 0){
        Serial.print(".");
        if (millis() - last_mode_button_time > MODE_BUTTON_DELAY * 1000){
            last_mode_button_time = 0;
            // Switch the current mode.  NORMAL => TIME => AFTER_TIME => TEMP => NORMAL
            if (mode == MODE_TEMP) mode = MODE_NORMAL;
            else mode++;
            Serial.print("Switched the current mode: "); Serial.println(mode);
        }
    }

    // We check status every 500ms
    if (cnt < 4){
        cnt++;
    }
    else {

        thermocouple_temp = thermocouple.readFarenheit(DEBUG);
        if (isnan(thermocouple_temp) || thermocouple_temp == 32.0 || thermocouple_temp <= 0) {
            displayString(" ERR");
            mcp23008.digitalWrite(RELAY_CHANNEL, LOW);
            Serial.println("Thermocouple is mulfunctioning...");
            tone(BUZZER_PIN, BUZZER_FREQUENCY_ERROR, 500);
            delay(500);
            return;
        }

        // Check thermocouple temperature every 10 sec
        if (cnt_thermocouple < TEMP_CHECK_INTERVAL * 2){
            cnt_thermocouple++;    
        }
        else {
            // Check the last temperature value & state of the relay to validate.
            if (last_relay_state) {
                if (last_thermocouple_temp > thermocouple_temp){
                    displayString(" ERR");
                    Serial.println("Temperature didn't raised though the relay was ON. Something is wrong?");
                    tone(BUZZER_PIN, BUZZER_FREQUENCY_ERROR, 500);
                    delay(500);
                    return;
                }
            } else {
                if (last_thermocouple_temp < thermocouple_temp){
                    displayString(" ERR");
                    Serial.println("Temperature didn't go down though the relay was OFF. Something is wrong?");
                    tone(BUZZER_PIN, BUZZER_FREQUENCY_ERROR, 500);
                    delay(500);
                    return;
                }
            }
                
            Serial.print("Current temperature("); Serial.print(thermocouple_temp);
            if (thermocouple_temp < threshold){
                Serial.print("°F) is lower than the threshold("); Serial.print(threshold); Serial.println("°F), turning relay ON...");
                mcp23008.digitalWrite(RELAY_CHANNEL, HIGH);
                last_relay_state = true;
            }
            else {
                Serial.print("°F) is higher than the threshold("); Serial.print(threshold); Serial.println("°F), turning relay OFF...");
                mcp23008.digitalWrite(RELAY_CHANNEL, LOW);
                last_relay_state = false;
            }
            cnt_thermocouple = 0;
            last_thermocouple_temp = thermocouple_temp;
        }

        // Read object temperature and check if the half is opened or closed.
        mlx_temp_obj = mlx.readObjectTempF(DEBUG);
        if (mlx_temp_obj <= 0 || mlx_temp_obj > 500){
            displayString(" ERR");
            mcp23008.digitalWrite(RELAY_CHANNEL, LOW);
            Serial.println("MLX90614 is mulfunctioning...");
            tone(BUZZER_PIN, BUZZER_FREQUENCY_ERROR, 500);
            delay(100);
            return;    
        }
        if (mlx_temp_obj > TEMP_THRESHOLD_HALF){
            if (!is_half_opened) Serial.println("Half is just opened.");
            is_half_opened = true;
        }
        else {
            if (is_half_opened) Serial.println("Half is just closed.");
            is_half_opened = false;
        }
        
        // Read the status of MPU6050 and check if the half is flipped or not.
        mpu6050_values = mpu6050Read(MPU_addr, DEBUG);
        // FIXME: Which axis should be used? AcX/AcY/AcZ
        if (mpu6050_values.AcX < -0.1){
            if (!is_half_flipped) Serial.println("Half is just flipped.");
            is_half_flipped = true;
        }
        else if (mpu6050_values.AcX > 0.1){
            if (is_half_flipped) Serial.println("Half is in the normal position.");
            is_half_flipped = false;
        }

        // Display current state every 5 sec.
        if ((millis() / 1000) % 5 == 0 && millis() % 1000 < 500) {
            Serial.print("State: "); Serial.print(state);
            Serial.print("   Thermocouple: "); Serial.print(thermocouple_temp); 
            Serial.print("°F   Object Temp: "); Serial.print(mlx_temp_obj);
            Serial.print("°F   AcX: "); Serial.println(mpu6050_values.AcX); 
        } 
        
        if (mode == MODE_NORMAL || mode == MOTE_AFTER_TIME){
            switch (state) {
                case STATE_READY: {         // 0
                    cnt_buzzer = 0;
                    if (is_half_flipped) Serial.println("Half is flipped! Please flip back to start heating...");
                    else if (is_half_opened) Serial.println("Half is opened! Please close to start heating...");
                    else state = STATE_HEATING;
                    break;
                }
                case STATE_HEATING: {       // 1
                    if (is_half_opened){
                        Serial.println("WARNING: Please close the half!");
                        tone(BUZZER_PIN, BUZZER_FREQUENCY_ERROR, 1000);
                    } 
                    else {
                        if (thermocouple_temp < threshold){
                            displayString("  LO");       
                        } 
                        else {
                            Serial.print("Temperature is reached to "); Serial.print(threshold);
                            Serial.println("°F, please open the half!");
                            state = STATE_REACHED;
                        }
                    }
                    break;
                }
                case STATE_REACHED: {       // 2
                    // The buzzer should give a series of 5 short buzzes (1 sec buzz with 2 seconds between buzzes.
                    if (cnt_buzzer < 5 && millis() - last_buzzer_time > 3000){
                        cnt_buzzer++;
                        last_buzzer_time = millis();
                        tone(BUZZER_PIN, BUZZER_FREQUENCY, 1000);
                    }
                    if (is_half_opened){
                        cnt_buzzer = 0;
                        Serial.println("Half is opened, please flip!");
                        displayString("FLIP");
                        state = STATE_OPENED;
                    }
                    else {
                        if (is_lcd_shown) displayString("OPEN");
                        else displayString("    ");
                        is_lcd_shown = !is_lcd_shown;
                    }
                    break;
                }
                case STATE_OPENED: {        // 3
                    if (is_half_flipped){
                        Serial.println("Half is flipped, please close!");
                        if (is_lcd_shown) displayString("CLOSE");
                        else displayString("    ");
                        is_lcd_shown = !is_lcd_shown;
                        state = STATE_FLIPPED;
                    }
                    else if (!is_half_opened){
                        Serial.println("WARNING: Please flip before closing!");
                        tone(BUZZER_PIN, BUZZER_FREQUENCY_ERROR, 1000);
                    }
                    break;
                }
                case STATE_FLIPPED: {       // 4
                    if (!is_half_opened){
                        Serial.println("Starting countdown...");
                        count_down_start_time = millis() / 1000;
                        state = STATE_COUNT_DOWN;
                    }
                    break;
                }
                case STATE_COUNT_DOWN: {    // 5
                    if (is_half_opened){
                        Serial.println("Half is flipped, please close!");
                        displayString("CLOSE");
                        tone(BUZZER_PIN, BUZZER_FREQUENCY_ERROR, 1000);
                    }
                    else {
                        remaining_time = cook_time - millis() / 1000 + count_down_start_time;
                        if (remaining_time % 10 == 0) {
                            Serial.print("Remaining time: "); Serial.println(remaining_time);
                        }
                        if (remaining_time > 0){
                            alpha4.writeDigitNum(0, remaining_time / 600);
                            alpha4.writeDigitNum(1, (remaining_time / 60) % 10, true);
                            alpha4.writeDigitNum(2, (remaining_time % 60) / 10);
                            alpha4.writeDigitNum(3, remaining_time % 10);
                            alpha4.writeDisplay();
                        }
                        else{
                            Serial.println("Finished!");
                            state = STATE_DONE;
                        }
                    }
                    break;
                }
                case STATE_DONE: {          // 6
                    if (is_lcd_shown) displayString("DONE");
                    else displayString("    ");
                    is_lcd_shown = !is_lcd_shown;
                    // Five 3 second buzzes with 2 seconds between buzzes.
                    if (cnt_buzzer < 5 && millis() - last_buzzer_time > 5000){
                        cnt_buzzer++;
                        last_buzzer_time = millis();
                        tone(BUZZER_PIN, BUZZER_FREQUENCY, 3000);
                    }
                    if (is_half_opened) state = STATE_READY;
                    break;
                }
            }
        }
        else {      // Blink the current temperature or cook_time value
            is_lcd_shown = !is_lcd_shown;
            if (is_lcd_shown){
                if (mode == MODE_TIME){
                    alpha4.writeDigitNum(0, (cook_time / 600));
                    alpha4.writeDigitNum(1, (cook_time / 60) % 10, true);
                    alpha4.writeDigitNum(2, (cook_time % 60) / 10);
                    alpha4.writeDigitNum(3, cook_time % 10);
                }
                else {
                    alpha4.writeDigitNum(0, threshold / 1000);
                    alpha4.writeDigitNum(1, (threshold / 100) % 10);
                    alpha4.writeDigitNum(2, (threshold / 10) % 10);
                    alpha4.writeDigitNum(3, threshold % 10);
                }
                alpha4.writeDisplay();
            }
            else {
                displayString("    ");
            }
        }
        cnt = 0;
    }

    delay(100);
}
